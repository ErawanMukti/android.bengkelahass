package com.belajar.akhmad_afandik.surabaya;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.belajar.akhmad_afandik.surabaya.List.ListBengkel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

public class AdminListBengkel extends AppCompatActivity {

    Context c;
    ListView listWisata;
    FloatingActionButton btnTambah;
    String ip_address;
    ArrayList<String> nama_bengkel;
    ArrayList<ListBengkel> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_list_bengkel);

        c          = this;
        ip_address = getResources().getString(R.string.ipaddress);
        listWisata = (ListView) findViewById(R.id.listAdmin);
        btnTambah  = (FloatingActionButton) findViewById(R.id.fabNew);

        btnTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(c, FormBengkel.class);
                startActivity(i);
            }
        });

        GetDataAdmin task = new GetDataAdmin();
        task.execute();
    }

    @Override
    protected void onResume() {
        super.onResume();
        GetDataAdmin task = new GetDataAdmin();
        task.execute();
    }

    class GetDataAdmin extends AsyncTask<String, Void, String> {
        private Dialog loadingDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadingDialog = ProgressDialog.show(c, "Harap Tunggu", "Mengambil Data...");
        }

        @Override
        protected String doInBackground(String... params) {
            String result = "";
            try {
                URL url = new URL(ip_address + "get_bengkel.php");
                URLConnection con = url.openConnection();
                con.setDoOutput(true);

                BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                result = sb.toString();
                reader.close();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result){
            loadingDialog.dismiss();

            nama_bengkel = new ArrayList<>();
            data         = new ArrayList<>();
            JSONArray js = null;

            if(result.isEmpty()){
                Toast.makeText(c, "Data tidak ditemukan", Toast.LENGTH_LONG).show();
            }else{
                String myJSON = result.trim();
                try {
                    JSONObject jsonObj = new JSONObject(myJSON);
                    js = jsonObj.getJSONArray("hasil");

                    for(int i=0; i<js.length(); i++){
                        JSONObject obj = js.getJSONObject(i);

                        nama_bengkel.add(obj.getString("nama"));

                        ListBengkel list = new ListBengkel();
                        list.setId(obj.getString("id"));
                        list.setNama(obj.getString("nama"));
                        list.setAlamat(obj.getString("alamat"));
                        list.setTelepon(obj.getString("tlp"));
                        list.setEmail(obj.getString("email"));
                        list.setJam(obj.getString("jam"));
                        list.setLat(obj.getDouble("lat"));
                        list.setLng(obj.getDouble("lng"));
                        list.setGambar(ip_address + "/images/" + obj.getString("gambar"));

                        data.add(list);
                    }

                    ArrayAdapter adapter = new ArrayAdapter(getBaseContext(), android.R.layout.simple_list_item_1, nama_bengkel);
                    listWisata.setAdapter(adapter);

                    listWisata.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Intent i = new Intent(getBaseContext(), FormBengkel.class);
                            i.putExtra("id", data.get(position).getId());
                            i.putExtra("nama", data.get(position).getNama());
                            i.putExtra("alamat", data.get(position).getAlamat());
                            i.putExtra("tlp", data.get(position).getTelepon());
                            i.putExtra("email", data.get(position).getEmail());
                            i.putExtra("jam", data.get(position).getJam());
                            i.putExtra("lat", data.get(position).getLat());
                            i.putExtra("lng", data.get(position).getLng());
                            i.putExtra("gambar", data.get(position).getGambar());

                            startActivity(i);
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
