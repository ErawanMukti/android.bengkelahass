package com.belajar.akhmad_afandik.surabaya.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.belajar.akhmad_afandik.surabaya.Fragment1;
import com.belajar.akhmad_afandik.surabaya.Fragment2;
import com.belajar.akhmad_afandik.surabaya.R;

/**
 * Created by akhmad-afandik on 20/05/17.
 */

public class ViewPagerAdapter extends FragmentPagerAdapter {

    final int PAGE_COUNT = 2;
    final Context context;

    public ViewPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;

        switch (position){
            case 0:
                fragment = Fragment1.newInstance();
                break;
            case 1:
                fragment = Fragment2.newInstance();
                break;
        }

        return fragment;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0:
                return context.getResources().getString(R.string.tab_1);
            case 1:
                return context.getResources().getString(R.string.tab_2);
        }

        return null;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

}
