package com.belajar.akhmad_afandik.surabaya;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class FormBengkel extends AppCompatActivity implements View.OnClickListener {

    Context c;
    Bundle extras;
    EditText txtNama, txtAlamat, txtTelepon, txtEmail, txtLatitude, txtLongitude, txtInfo;
    Button btnSimpan, btnHapus;
    String ip_address, id, nama, alamat, telepon, email, gambar;
    Double lat, lng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_bengkel);

        c            = this;
        extras       = getIntent().getExtras();
        ip_address   = getResources().getString(R.string.ipaddress);
        txtNama      = (EditText) findViewById(R.id.txtFormNama);
        txtAlamat    = (EditText) findViewById(R.id.txtFormAlamat);
        txtTelepon   = (EditText) findViewById(R.id.txtFormTelepon);
        txtEmail     = (EditText) findViewById(R.id.txtFormEmail);
        txtLatitude  = (EditText) findViewById(R.id.txtFormLat);
        txtLongitude = (EditText) findViewById(R.id.txtFormLng);
        btnSimpan    = (Button) findViewById(R.id.btnFormSimpan);
        btnHapus     = (Button) findViewById(R.id.btnFormHapus);

        try {
            id      = extras.getString("id");
            nama    = extras.getString("nama");
            alamat  = extras.getString("alamat");
            telepon = extras.getString("telepon");
            email   = extras.getString("email");
            lat     = extras.getDouble("lat");
            lng     = extras.getDouble("lng");
            gambar  = extras.getString("gambar");
        } catch (NullPointerException ex) {
            id = "";
        }

        if ( id.equals("") ) {
            lat = 0.0;
            lng = 0.0;
            btnHapus.setVisibility(View.GONE);
        }

        txtNama.setText(nama);
        txtAlamat.setText(alamat);
        txtTelepon.setText(telepon);
        txtEmail.setText(email);
        txtLatitude.setText(String.valueOf(lat));
        txtLongitude.setText(String.valueOf(lng));

        btnSimpan.setOnClickListener(this);
        btnHapus.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnFormSimpan:
                nama    = txtNama.getText().toString();
                alamat  = txtAlamat.getText().toString();
                telepon = txtAlamat.getText().toString();
                email   = txtAlamat.getText().toString();
                lat     = Double.valueOf(txtLatitude.getText().toString());
                lng     = Double.valueOf(txtLongitude.getText().toString());

                SimpanBengkel task_simpan = new SimpanBengkel();
                task_simpan.execute();
                break;

            case R.id.btnFormHapus:
                HapusBengkel task_hapus = new HapusBengkel();
                task_hapus.execute();
                break;
        }

    }

    class HapusBengkel extends AsyncTask<String, Void, String> {
        private Dialog loadingDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadingDialog = ProgressDialog.show(c, "Harap Tunggu", "Menghapus Data...");
        }

        @Override
        protected String doInBackground(String... params) {
            String result = "";
            try {
                String data = "id=" + id;

                URL url = new URL(ip_address + "hapus_bengkel.php");
                URLConnection con = url.openConnection();
                con.setDoOutput(true);

                OutputStreamWriter writer = new OutputStreamWriter(con.getOutputStream());
                writer.write(data);
                writer.flush();

                BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                result = sb.toString();
                reader.close();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result){
            loadingDialog.dismiss();

            if(result.isEmpty()){
                Toast.makeText(getApplicationContext(), "Data tidak ditemukan", Toast.LENGTH_LONG).show();
            }else{
                String s = result.trim();

                if (s.equalsIgnoreCase("Berhasil")) {
                    Toast.makeText(getBaseContext(), "Data Berhasil Dihapus", Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Toast.makeText(getBaseContext(), s, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    class SimpanBengkel extends AsyncTask<String, Void, String> {
        private Dialog loadingDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadingDialog = ProgressDialog.show(c, "Harap Tunggu", "Menyimpan Data...");
        }

        @Override
        protected String doInBackground(String... params) {
            String result = "";
            try {
                String data = "id=" + id;
                data += "&nama=" + nama;
                data += "&alamat=" + alamat;
                data += "&telepon=" + telepon;
                data += "&email=" + email;
                data += "&lat=" + lat;
                data += "&lng=" + lng;

                URL url = new URL(ip_address + "simpan_bengkel.php");
                URLConnection con = url.openConnection();
                con.setDoOutput(true);

                OutputStreamWriter writer = new OutputStreamWriter(con.getOutputStream());
                writer.write(data);
                writer.flush();

                BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                result = sb.toString();
                reader.close();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result){
            loadingDialog.dismiss();

            if(result.isEmpty()){
                Toast.makeText(getApplicationContext(), "Data tidak ditemukan", Toast.LENGTH_LONG).show();
            }else{
                String s = result.trim();

                if (s.contains("Berhasil")) {
                    Toast.makeText(getBaseContext(), "Data Berhasil Disimpan", Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Toast.makeText(getBaseContext(), s, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

}
