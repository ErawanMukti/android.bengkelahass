package com.belajar.akhmad_afandik.surabaya;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.belajar.akhmad_afandik.surabaya.List.ListBengkel;
import com.belajar.akhmad_afandik.surabaya.adapter.EmptyRecViewAdapter;
import com.belajar.akhmad_afandik.surabaya.adapter.RecViewAdapter;
import com.belajar.akhmad_afandik.surabaya.app.AppController;

import java.util.ArrayList;

/**
 * Created by akhmad-afandik on 20/05/17.
 */

public class Fragment2 extends Fragment {

    Context c;
    RecyclerView rv;
    ArrayList<ListBengkel> list_bengkel;
    RecViewAdapter adapter;
    Handler h;
    EmptyRecViewAdapter emptyAdapter;

    public static Fragment2 newInstance(){
        return new Fragment2();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment2, container, false);

        c = getActivity();

        rv = (RecyclerView) rootView.findViewById(R.id.list);
        rv.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(c, LinearLayoutManager.VERTICAL, false);
        rv.setLayoutManager(layoutManager);
        emptyAdapter = new EmptyRecViewAdapter();
        rv.setAdapter(emptyAdapter);

        h = new Handler();
        h.postDelayed(m, 3000);

        return rootView;
    }

    Runnable m = new Runnable() {
        @Override
        public void run() {
            list_bengkel = ((AppController) getActivity().getApplicationContext()).getListBengkel();
            if ( list_bengkel.size() > 0 ) {
                adapter = new RecViewAdapter(c, list_bengkel);
                rv.setAdapter(adapter);
                adapter.sortDistance();
            } else {
                h = new Handler();
                h.postDelayed(m, 1000);
            }
        }
    };
}
