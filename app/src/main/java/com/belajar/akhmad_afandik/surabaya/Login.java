package com.belajar.akhmad_afandik.surabaya;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Login extends AppCompatActivity {

    EditText txtUsername, txtPassword;
    Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        txtUsername = (EditText) findViewById(R.id.txtUsername);
        txtPassword = (EditText) findViewById(R.id.txtPassword);
        btnLogin    = (Button) findViewById(R.id.btnLogin);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String uname, upass;

                uname = txtUsername.getText().toString();
                upass = txtPassword.getText().toString();

                if ( uname.equals("") ) {
                    Toast.makeText(getBaseContext(), "Kolom username tidak boleh kosong", Toast.LENGTH_SHORT).show();
                    return;
                }

                if ( upass.equals("") ) {
                    Toast.makeText(getBaseContext(), "Kolom password tidak boleh kosong", Toast.LENGTH_SHORT).show();
                    return;
                }

                if ( uname.equals("admin") && upass.equals("admin-123") ) {
                    Intent i = new Intent(getBaseContext(), AdminListBengkel.class);
                    startActivity(i);
                    finish();
                } else {
                    Toast.makeText(getBaseContext(), "Username / password yang anda masukkan salah.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
