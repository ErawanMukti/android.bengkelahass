package com.belajar.akhmad_afandik.surabaya;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;
import java.util.TimeZone;

/**
 * Created by akhmad-afandik on 20/05/17.
 */

public class Booking extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    Context c;
    Bundle extras;
    EditText nm, almt, kntk, motor, plat_nmr, kilometer, keluh;
    TextView date, time;
    Button reset, sent;
    CheckBox check1, check2, check3, check4, check5, check6, check7, check8, check9;
    StringBuffer pilih_service;
    String email;
    int selected_day, selected_month, selected_year, selected_hour, selected_minutes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.booking);

        c = this;
        extras = getIntent().getExtras();
        nm = (EditText) findViewById(R.id.nama);
        almt = (EditText) findViewById(R.id.alamat);
        kntk = (EditText) findViewById(R.id.kontak);
        motor = (EditText) findViewById(R.id.jenis);
        plat_nmr = (EditText) findViewById(R.id.plat);
        kilometer = (EditText) findViewById(R.id.spido);
        keluh = (EditText) findViewById(R.id.keluhan);
        date = (TextView) findViewById(R.id.tanggal);
        time = (TextView) findViewById(R.id.waktu);
        check1 = (CheckBox) findViewById(R.id.checkbox1);
        check2 = (CheckBox) findViewById(R.id.checkbox2);
        check3 = (CheckBox) findViewById(R.id.checkbox3);
        check4 = (CheckBox) findViewById(R.id.checkbox4);
        check5 = (CheckBox) findViewById(R.id.checkbox5);
        check6 = (CheckBox) findViewById(R.id.checkbox6);
        check7 = (CheckBox) findViewById(R.id.checkbox7);
        check8 = (CheckBox) findViewById(R.id.checkbox8);
        check9 = (CheckBox) findViewById(R.id.checkbox9);
        reset = (Button) findViewById(R.id.btn_reset);
        sent = (Button) findViewById(R.id.btn_sent);
        email = extras.getString("email");

        sent.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View v) {
                if (nm.length() == 0) {
                    Toast.makeText(getBaseContext(), "Kolom Nama Pemesan Wajib Diisi !!", Toast.LENGTH_LONG).show();
                } else if (almt.length() == 0) {
                    Toast.makeText(getBaseContext(), "Kolom Alamat Pemesan Wajib Diisi !!", Toast.LENGTH_LONG).show();
                } else if (kntk.length() == 0) {
                    Toast.makeText(getBaseContext(), "Kolom Nomor HP Wajib Diisi !!", Toast.LENGTH_LONG).show();
                } else if (motor.length() == 0) {
                    Toast.makeText(getBaseContext(), "Kolom Tipe Motor Wajib Diisi !!", Toast.LENGTH_LONG).show();
                } else if (plat_nmr.length() == 0) {
                    Toast.makeText(getBaseContext(), "Kolom Plat Nomor Wajib Diisi !!", Toast.LENGTH_LONG).show();
                } else if (kilometer.length() == 0) {
                    Toast.makeText(getBaseContext(), "Kolom Spidometer Wajib Diisi !!", Toast.LENGTH_LONG).show();
                } else if (date.length() == 0) {
                    Toast.makeText(getBaseContext(), "Kolom Tanggal Wajib Diisi !!", Toast.LENGTH_LONG).show();
                } else if (time.length() == 0) {
                    Toast.makeText(getBaseContext(), "Kolom Jam Wajib Diisi !!", Toast.LENGTH_LONG).show();
                } else if (keluh.length() == 0) {
                    Toast.makeText(getBaseContext(), "Kolom Keluhan Service Wajib Diisi !!", Toast.LENGTH_LONG).show();
                } else {
                    pilih_service = new StringBuffer();
                    if (check1.isChecked()) {
                        pilih_service.append("\n - Service Rutin");
                    }
                    if (check2.isChecked()) {
                        pilih_service.append("\n - Ganti Busi");
                    }
                    if (check3.isChecked()) {
                        pilih_service.append("\n - Ganti Oli Mesin");
                    }
                    if (check4.isChecked()) {
                        pilih_service.append("\n - Ganti Oli Rem");
                    }
                    if (check5.isChecked()) {
                        pilih_service.append("\n - Ganti Bolam");
                    }
                    if (check6.isChecked()) {
                        pilih_service.append("\n - Ganti Campas Rem");
                    }
                    if (check7.isChecked()) {
                        pilih_service.append("\n - Ganti Ban Dalam");
                    }
                    if (check8.isChecked()) {
                        pilih_service.append("\n - Ganti Ban Luar");
                    }
                    if (check9.isChecked()) {
                        pilih_service.append("\n - Lain-lain");
                    }

                    String txt = "Booking Service Bengkel AHASS"
                            + "\n\n" + "Nama : " + nm.getText().toString() + "\n" + "Alamat : "
                            + almt.getText().toString() + "\n" + "Kontak : " + kntk.getText().toString()
                            + "\n" + "Jenis Motor : " + motor.getText().toString() + "\n" + "Nomor Polisi : "
                            + plat_nmr.getText().toString() + "\n" + "Kilometer Motor : "
                            + kilometer.getText().toString() + "\n" + "Tanggal : " + date.getText().toString()
                            + "\n" + "Jam : " + time.getText().toString() + "\n" + "Detail Service :"
                            + pilih_service.toString() + "\n" + "Detail Keluhan : " + "\n" + keluh.getText().toString();

                    String[] to = {email};

                    Intent mailIntent = new Intent(Intent.ACTION_SEND);
                    mailIntent.setData(Uri.parse("mailto:"));
                    mailIntent.setType("text/plain");
                    mailIntent.putExtra(Intent.EXTRA_EMAIL, to);
                    mailIntent.putExtra(Intent.EXTRA_SUBJECT, "Booking Service");
                    mailIntent.putExtra(Intent.EXTRA_TEXT, txt);
                    startActivityForResult(mailIntent.createChooser(mailIntent, "Pilih Aplikasi Untuk Mengirim:"), 1);
                }
            }
        });

        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nm.setText("");
                almt.setText("");
                kntk.setText("");
                motor.setText("");
                plat_nmr.setText("");
                kilometer.setText("");
                date.setText("");
                time.setText("");
                keluh.setText("");
                check1.setChecked(false);
                check2.setChecked(false);
                check3.setChecked(false);
                check4.setChecked(false);
                check5.setChecked(false);
                check6.setChecked(false);
                check7.setChecked(false);
                check8.setChecked(false);
                check9.setChecked(false);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            Toast.makeText(c, "Email terkirim", Toast.LENGTH_SHORT).show();

            long calendarId = 1;
            Calendar dtStart = Calendar.getInstance();
            dtStart.set(selected_year, selected_month, selected_day, selected_hour, selected_minutes);
            long startMillis = dtStart.getTimeInMillis();

            ContentResolver cr = c.getContentResolver();
            ContentValues cv   = new ContentValues();

            cv.put(CalendarContract.Events.DTSTART, startMillis);
            cv.put(CalendarContract.Events.DURATION, "PT1H");
            cv.put(CalendarContract.Events.TITLE, "AHASS - Ayo Service");
            cv.put(CalendarContract.Events.DESCRIPTION, "Service Motor Rutin");
            cv.put(CalendarContract.Events.CALENDAR_ID, calendarId);
            cv.put(CalendarContract.Events.EVENT_TIMEZONE, TimeZone.getDefault().getID());
            cv.put(CalendarContract.Events.RRULE, "FREQ=MONTHLY;COUNT=4");
            cv.put(CalendarContract.Events.HAS_ALARM, 1);
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            try{
                Uri uri = cr.insert(CalendarContract.Events.CONTENT_URI, cv);
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }

            Toast.makeText(c, "Schedule berhasil dibuat. Silahkan periksa kalendar anda", Toast.LENGTH_SHORT).show();
        }
    }

    public void showDatePickerDialog(View view) {
        final Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);

        new DatePickerDialog(c, this, year, month, day).show();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        date.setText(dayOfMonth+"/"+(++month)+"/"+year);
        selected_day = dayOfMonth;
        selected_month = month;
        selected_year = year;
    }

    public void showTimePickerDialog(View view) {
        final Calendar cal = Calendar.getInstance();
        int hour = cal.get(Calendar.HOUR_OF_DAY);
        int minute = cal.get(Calendar.MINUTE);
        int second = cal.get(Calendar.SECOND);

        new TimePickerDialog(c, this, hour, minute,
                DateFormat.is24HourFormat(c)).show();
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        time.setText(hourOfDay+" : "+(++minute));
        selected_hour = hourOfDay;
        selected_minutes = minute;
    }
}
