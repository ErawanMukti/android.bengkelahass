package com.belajar.akhmad_afandik.surabaya.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.belajar.akhmad_afandik.surabaya.InfoBengkel;
import com.belajar.akhmad_afandik.surabaya.List.ListBengkel;
import com.belajar.akhmad_afandik.surabaya.List.ListReview;
import com.belajar.akhmad_afandik.surabaya.R;
import com.squareup.picasso.Picasso;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class ReviewAdapter extends RecyclerView.Adapter<ReviewAdapter.ViewHolder> {

    private Context context;
    private ArrayList<ListReview> data;

    public ReviewAdapter(Context c, ArrayList<ListReview> data) {
        this.context = c;
        this.data = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.review_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.txtRowReviewIsi.setText(data.get(position).getIsi());
        holder.txtRowReviewNama.setText(data.get(position).getNama());
        holder.txtRowReviewTgl.setText(data.get(position).getTgl());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtRowReviewIsi, txtRowReviewNama, txtRowReviewTgl;

        public ViewHolder(View view) {
            super(view);

            txtRowReviewIsi  = (TextView) view.findViewById(R.id.lblRowReviewContent);
            txtRowReviewNama = (TextView) view.findViewById(R.id.lblRowReviewName);
            txtRowReviewTgl  = (TextView) view.findViewById(R.id.lblRowReviewTgl);
        }
    }

}
