package com.belajar.akhmad_afandik.surabaya.app;

import android.app.Application;

import com.belajar.akhmad_afandik.surabaya.List.ListBengkel;

import java.util.ArrayList;

/**
 * Created by akhmad-afandik on 21/06/17.
 */

public class AppController extends Application {
    private ArrayList<ListBengkel> listBengkel;

    public void setListBengkel() {
        listBengkel = new ArrayList<>();
    }

    public ArrayList<ListBengkel> getListBengkel() {
        return listBengkel;
    }

}
