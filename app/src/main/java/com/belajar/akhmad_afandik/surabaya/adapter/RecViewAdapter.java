package com.belajar.akhmad_afandik.surabaya.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.belajar.akhmad_afandik.surabaya.InfoBengkel;
import com.belajar.akhmad_afandik.surabaya.List.ListBengkel;
import com.belajar.akhmad_afandik.surabaya.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class RecViewAdapter extends RecyclerView.Adapter<RecViewAdapter.ViewHolder> {

    private Context context;
    private ArrayList<ListBengkel> data;

    public RecViewAdapter(Context c, ArrayList<ListBengkel> data) {
        this.context = c;
        this.data = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment2_list_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.txtRowNama.setText(data.get(position).getNama());

        long jarak = Math.round(data.get(position).getDistance());
        holder.txtRowJarak.setText(String.valueOf(jarak) + " KM");

        Uri uri = Uri.parse(data.get(position).getGambar().replace(" ", "%20"));
        Picasso.with(context).load(uri).resize(240, 240).into(holder.imgRowThumb);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView txtRowNama, txtRowJarak;
        private ImageView imgRowThumb;

        public ViewHolder(View view) {
            super(view);

            txtRowNama  = (TextView) view.findViewById(R.id.nama);
            txtRowJarak = (TextView) view.findViewById(R.id.jarak);
            imgRowThumb = (ImageView) view.findViewById(R.id.gambar);
            context = view.getContext();

            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent i = new Intent(context, InfoBengkel.class);
            i.putExtra("idx", getAdapterPosition());
            /*i.putExtra("id", data.get(getAdapterPosition()).getId());
            i.putExtra("nama", data.get(getAdapterPosition()).getNama());
            i.putExtra("alamat", data.get(getAdapterPosition()).getAlamat());
            i.putExtra("tlp", data.get(getAdapterPosition()).getTelepon());
            i.putExtra("email", data.get(getAdapterPosition()).getEmail());
            i.putExtra("lat", data.get(getAdapterPosition()).getLat());
            i.putExtra("lng", data.get(getAdapterPosition()).getLng());
            i.putExtra("gambar", data.get(getAdapterPosition()).getGambar());
            i.putExtra("jarak", data.get(getAdapterPosition()).getDistance());*/
            context.startActivity(i);
        }
    }

    public void sortDistance() {
        Collections.sort(data, new Comparator<ListBengkel>() {
            @Override
            public int compare(ListBengkel o1, ListBengkel o2) {
                return o1.getDistance().compareTo(o2.getDistance());
            }
        });
        notifyDataSetChanged();
    }

}
