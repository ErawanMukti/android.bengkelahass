package com.belajar.akhmad_afandik.surabaya;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.belajar.akhmad_afandik.surabaya.List.ListBengkel;
import com.belajar.akhmad_afandik.surabaya.adapter.ViewPagerAdapter;
import com.belajar.akhmad_afandik.surabaya.app.AppController;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    Context c;
    String ip_address;
    Toolbar toolbar;
    TabLayout tabLayout;
    ViewPager viewPager;
    FloatingActionButton fabRefresh;
    ArrayList<ListBengkel> list_bengkel;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    LocationRequest mLocationRequest;
    Double curLat = 0.00, curLng = 0.00;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        c            = this;
        toolbar      = (Toolbar) findViewById(R.id.toolbar_info);
        tabLayout    = (TabLayout) findViewById(R.id.tabs);
        viewPager    = (ViewPager) findViewById(R.id.viewPager);
        fabRefresh   = (FloatingActionButton) findViewById(R.id.fabRefresh);
        ip_address   = getResources().getString(R.string.ipaddress);

        ((AppController) getApplicationContext()).setListBengkel();
        list_bengkel = ((AppController) getApplicationContext()).getListBengkel();

        setupToolbar();
        init();

        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(c)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
        if (!mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
        }

        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)
                .setFastestInterval(1 * 1000);

        fabRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetDataJSON task = new GetDataJSON();
                task.execute();
            }
        });

        Handler h = new Handler();
        h.postDelayed(m, 2000);

    }

    protected void setupToolbar() {
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    private void init(){
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupViewPager(final ViewPager viewPager) {
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), this);
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.setOffscreenPageLimit(2);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.setTabsFromPagerAdapter(viewPagerAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item){
        int id = item.getItemId();
        if (id == R.id.action_about) {
            Intent i = new Intent(this, About.class);
            startActivity(i);
            return true;
        }

        if (id == R.id.action_help) {
            Intent i = new Intent(this, Bantuan.class);
            startActivity(i);
            return true;
        }

        if (id == R.id.action_kritik) {
            Intent i = new Intent(this, Critics.class);
            startActivity(i);
            return true;
        }

        if (id == R.id.action_login) {
            Intent i = new Intent(this, Login.class);
            startActivity(i);
            return true;
        }
        if (id == R.id.action_exit) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(c, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(c, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(c, "Tidak dapat mengambil lokasi", Toast.LENGTH_SHORT).show();
            return;
        }

        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            handleNewLocation(mLastLocation);
        } else {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        handleNewLocation(location);
    }

    private void handleNewLocation(Location location) {
        curLat = location.getLatitude();
        curLng = location.getLongitude();
    }

    Runnable m = new Runnable() {
        @Override
        public void run() {
            GetDataJSON task = new GetDataJSON();
            task.execute();
        }
    };

    class GetDataJSON extends AsyncTask<String, Void, String> {
        private Dialog loadingDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadingDialog = ProgressDialog.show(c, "Harap Tunggu", "Mengambil Data...");
        }

        @Override
        protected String doInBackground(String... params) {
            String result = "";
            try {
                URL url = new URL(ip_address + "get_bengkel.php");
                URLConnection con = url.openConnection();
                con.setDoOutput(true);

                BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                result = sb.toString();
                reader.close();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result){
            loadingDialog.dismiss();

            if (curLat == 0.00 || curLng == 0.00) {
                Toast.makeText(c, "Gagal mendapatkan lokasi. Silahkan refresh kembali", Toast.LENGTH_SHORT).show();
                return;
            }

            if(result.isEmpty()){
                Toast.makeText(c, "Data tidak ditemukan", Toast.LENGTH_LONG).show();
            }else{
                JSONArray data = null;
                String myJSON = result.trim();

                try {
                    JSONObject jsonObj = new JSONObject(myJSON);
                    data = jsonObj.getJSONArray("hasil");

                    for(int i=0; i<data.length(); i++){
                        JSONObject obj = data.getJSONObject(i);

                        ListBengkel list = new ListBengkel();
                        list.setId(obj.getString("id"));
                        list.setNama(obj.getString("nama"));
                        list.setAlamat(obj.getString("alamat"));
                        list.setTelepon(obj.getString("tlp"));
                        list.setEmail(obj.getString("email"));
                        list.setJam(obj.getString("jam"));
                        list.setLat(obj.getDouble("lat"));
                        list.setLng(obj.getDouble("lng"));
                        list.setDistance(hitungJarak(new LatLng(curLat, curLng), new LatLng(obj.getDouble("lat"), obj.getDouble("lng"))));
                        list.setGambar(ip_address + "/images/" + obj.getString("gambar"));
                        list_bengkel.add(list);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private double hitungJarak(LatLng fromPosition, LatLng toPosition) {
        double dLat = Math.toRadians(toPosition.latitude - fromPosition.latitude);
        double dLon = Math.toRadians(toPosition.longitude - fromPosition.longitude);

        double lat1 = Math.toRadians(fromPosition.latitude);
        double lat2 = Math.toRadians(toPosition.latitude);

        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);

        double b = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        double R = 6371000;

        double inM = R * b;

        double inKm = inM / 1000;

        return inKm;
    }
}
