package com.belajar.akhmad_afandik.surabaya;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.VideoView;

/**
 * Created by akhmad-afandik on 04/06/17.
 */

public class Critics extends AppCompatActivity {

    Button kirim;
    VideoView videoView;

    @Override
    protected void onCreate(Bundle saveInstanState) {
        super.onCreate(saveInstanState);
        setContentView(R.layout.form_critics);

        kirim = (Button)findViewById(R.id.kirim);
        kirim.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto","fandy.pisces.fp@gmail.com", null));
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Kritik dan saran");
                startActivity(Intent.createChooser(emailIntent, "Silahkan kirim pesan ke email berikut..."));
            }
        });

      //  getSupportActionBar().setTitle("Video pendek");

        videoView = (VideoView) findViewById(R.id.video);

        videoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.ahass));
        //digunakan untuk mengidentifikasi resource seperti lokal video

        videoView.setMediaController(new MediaController(this));
        //menampilkan media controller video

        videoView.start();
        //memulai video

    }

}
