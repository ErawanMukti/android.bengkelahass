package com.belajar.akhmad_afandik.surabaya;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by akhmad-afandik on 21/05/17.
 */

public class About extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
        setContentView(R.layout.about);
    }
}
