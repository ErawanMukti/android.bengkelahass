package com.belajar.akhmad_afandik.surabaya;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.belajar.akhmad_afandik.surabaya.List.ListBengkel;
import com.belajar.akhmad_afandik.surabaya.List.ListReview;
import com.belajar.akhmad_afandik.surabaya.adapter.EmptyRecViewAdapter;
import com.belajar.akhmad_afandik.surabaya.adapter.ReviewAdapter;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

public class Review extends AppCompatActivity implements View.OnClickListener {

    Context c;
    Bundle extras;
    Toolbar tbReview;
    RecyclerView rvReview;
    FloatingActionButton fabReview;
    ArrayList<ListReview> list_review;
    ReviewAdapter adapter;
    EmptyRecViewAdapter emptyAdapter;
    String ip_address;
    int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review);

        c          = this;
        ip_address = getResources().getString(R.string.ipaddress);
        extras     = getIntent().getExtras();
        tbReview   = (Toolbar) findViewById(R.id.tbReview);
        rvReview   = (RecyclerView) findViewById(R.id.rvReview);
        fabReview  = (FloatingActionButton) findViewById(R.id.fabReview);
        id         = Integer.valueOf(extras.getString("id"));

        rvReview.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(c, LinearLayoutManager.VERTICAL, false);
        rvReview.setLayoutManager(layoutManager);

        setSupportActionBar(tbReview);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        fabReview.setOnClickListener(this);

        GetDataReview task = new GetDataReview();
        task.execute();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menu) {
        switch (menu.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(menu);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fabReview:
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                );

                final EditText txtNama = new EditText(c);
                txtNama.setHint("Nama");
                txtNama.setLayoutParams(lp);

                final EditText txtEmail = new EditText(c);
                txtEmail.setHint("Email");
                txtEmail.setLayoutParams(lp);

                final EditText txtIsi = new EditText(c);
                txtIsi.setHint("Review");
                txtIsi.setLayoutParams(lp);
                txtIsi.setLines(3);

                LinearLayout ll = new LinearLayout(c);
                ll.setOrientation(LinearLayout.VERTICAL);
                ll.setPadding(8,0,8,0);

                ll.addView(txtNama);
                ll.addView(txtEmail);
                ll.addView(txtIsi);

                AlertDialog.Builder builder = new AlertDialog.Builder(c);
                builder.setTitle("Review")
                        .setMessage("Silahkan tuliskan review anda untuk bengkel ini:")
                        .setView(ll)
                        .setPositiveButton("Simpan", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String nama   = txtNama.getText().toString();
                                String email  = txtEmail.getText().toString();
                                String isi    = txtIsi.getText().toString();

                                dialog.dismiss();
                                simpan(nama, email, isi);
                            }
                        })
                        .setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
                break;
        }

    }

    class GetDataReview extends AsyncTask<String, Void, String> {
        private Dialog loadingDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadingDialog = ProgressDialog.show(c, "Harap Tunggu", "Mengambil Data...");
        }

        @Override
        protected String doInBackground(String... params) {
            String result = "";
            try {
                String data = "id=" + id;

                URL url = new URL(ip_address + "get_review.php");
                URLConnection con = url.openConnection();
                con.setDoOutput(true);

                OutputStreamWriter writer = new OutputStreamWriter(con.getOutputStream());
                writer.write(data);
                writer.flush();

                BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                result = sb.toString();
                reader.close();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result){
            loadingDialog.dismiss();

            if(result.isEmpty()){
                Toast.makeText(c, "Data tidak ditemukan", Toast.LENGTH_LONG).show();
            }else{
                JSONArray data = null;
                String myJSON = result.trim();

                try {
                    JSONObject jsonObj = new JSONObject(myJSON);
                    data = jsonObj.getJSONArray("hasil");
                    list_review = new ArrayList<>();

                    for(int i=0; i<data.length(); i++){
                        JSONObject obj = data.getJSONObject(i);

                        ListReview list = new ListReview();
                        list.setTgl(obj.getString("tgl"));
                        list.setNama(obj.getString("nama"));
                        list.setEmail(obj.getString("email"));
                        list.setIsi(obj.getString("isi"));
                        list_review.add(list);
                    }

                    if (list_review.size() > 0) {
                        adapter = new ReviewAdapter(c, list_review);
                        rvReview.setAdapter(adapter);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void simpan(String nama, String email, String isi) {
        class ClsAsync extends AsyncTask<String, Void, String> {
            private Dialog loadingDialog;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loadingDialog = ProgressDialog.show(c, "Harap Tunggu", "Menyimpan Review...");
            }

            @Override
            protected String doInBackground(String... params) {
                String nama     = params[0];
                String email    = params[1];
                String isi      = params[2];

                String result = "";
                try {
                    String data = "id_bengkel=" + id;
                    data += "&nama=" + nama;
                    data += "&email=" + email;
                    data += "&isi=" + isi;

                    URL url = new URL(ip_address + "simpan_review.php");
                    URLConnection con = url.openConnection();
                    con.setDoOutput(true);

                    OutputStreamWriter writer = new OutputStreamWriter(con.getOutputStream());
                    writer.write(data);
                    writer.flush();

                    BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    result = sb.toString();
                    reader.close();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return result;
            }

            @Override
            protected void onPostExecute(String result) {
                loadingDialog.dismiss();

                if (result.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Terjadi kesalahan, silahkan ulangi", Toast.LENGTH_LONG).show();
                } else {
                    String s = result.trim();
                    if (s.equalsIgnoreCase("Berhasil")) {
                        finish();
                        startActivity(getIntent());
                    } else {
                        Toast.makeText(getApplicationContext(), s, Toast.LENGTH_LONG).show();
                    }
                }
            }
        }
        ClsAsync task = new ClsAsync();
        task.execute(nama, email, isi);
    }

}
