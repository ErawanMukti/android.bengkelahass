package com.belajar.akhmad_afandik.surabaya;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.belajar.akhmad_afandik.surabaya.List.ListBengkel;
import com.belajar.akhmad_afandik.surabaya.app.AppController;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.*;
import com.google.android.gms.maps.model.Marker;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by akhmad-afandik on 20/05/17.
 */

public class Fragment1 extends Fragment implements
        OnMapReadyCallback {

    Context c;
    MapView mpLokasi;
    GoogleMap gMap;
    String ip_address;
    ArrayList<ListBengkel> list_bengkel;
    Map<String, Integer> arrIdx;
    Handler h;

    public static Fragment1 newInstance() {
        return new Fragment1();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment1, container, false);

        c = getActivity();
        mpLokasi   = (MapView) rootView.findViewById(R.id.map);
        ip_address = getResources().getString(R.string.ipaddress);

        mpLokasi.onCreate(savedInstanceState);
        mpLokasi.getMapAsync(this);

        h = new Handler();
        h.postDelayed(m, 2500);

        return rootView;
    }

    @Override
    public void onPause() {
        super.onPause();
        mpLokasi.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mpLokasi.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        mpLokasi.onResume();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mpLokasi.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mpLokasi.onLowMemory();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        gMap = googleMap;

        LatLng posisi = new LatLng(-7.254177, 112.746806);
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setMapToolbarEnabled(true);
        if (ActivityCompat.checkSelfPermission(c, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(c, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        googleMap.setMyLocationEnabled(true);
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(posisi));
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(posisi, 12.0f));

        gMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                int idx = arrIdx.get(marker.getTitle());
                Intent inInfo = new Intent(c, InfoBengkel.class);
                inInfo.putExtra("idx", idx);
                startActivity(inInfo);

                return false;
            }
        });

    }

    Runnable m = new Runnable() {
        @Override
        public void run() {
            if ( load_data() == false ) {
                h = new Handler();
                h.postDelayed(m, 1000);
            }
        }
    };

    private boolean load_data() {
        try{
            list_bengkel = ((AppController) getActivity().getApplicationContext()).getListBengkel();
            if ( list_bengkel.size() > 0 ) {
                gMap.clear();
                arrIdx = new HashMap<>();

                for(int i=0; i<list_bengkel.size(); i++){
                    gMap.addMarker(new MarkerOptions()
                            .position(new LatLng(list_bengkel.get(i).getLat(), list_bengkel.get(i).getLng()))
                            .title(list_bengkel.get(i).getNama())
                            .snippet(list_bengkel.get(i).getAlamat())
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_icon_ahass)));

                    arrIdx.put(list_bengkel.get(i).getNama(), i);
                }
                return true;
            }
        } catch (NullPointerException ex) {
        }
        return false;
    }
}
