package com.belajar.akhmad_afandik.surabaya;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.belajar.akhmad_afandik.surabaya.List.ListBengkel;
import com.belajar.akhmad_afandik.surabaya.app.AppController;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class InfoBengkel extends AppCompatActivity implements View.OnClickListener {

    Context c;
    Bundle extras;
    ImageView imgToolbar;
    Toolbar toolbar;
    Button btnTlp, btnBooking, btnArah;
    TextView lblAlamat, lblJam, lblTelepon, lblEmail;
    ArrayList<ListBengkel> list_bengkel;
    int idx;
    String id, gambar, nama, alamat, jam, telepon, email;
    Double lat, lng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_bengkel);

        c            = this;
        extras       = getIntent().getExtras();
        imgToolbar   = (ImageView) findViewById(R.id.app_bar_image_info);
        toolbar      = (Toolbar) findViewById(R.id.toolbar_info);
        btnTlp       = (Button) findViewById(R.id.btnTeleponInfo);
        btnBooking   = (Button) findViewById(R.id.btnEmailInfo);
        btnArah      = (Button) findViewById(R.id.btnDirectionInfo);
        lblAlamat    = (TextView) findViewById(R.id.lblAlamatInfo);
        lblJam       = (TextView) findViewById(R.id.lblJamInfo);
        lblTelepon   = (TextView) findViewById(R.id.lblTeleponInfo);
        lblEmail     = (TextView) findViewById(R.id.lblEmailInfo);
        list_bengkel = ((AppController) getApplicationContext()).getListBengkel();
        idx          = extras.getInt("idx");
        id           = list_bengkel.get(idx).getId();
        gambar       = list_bengkel.get(idx).getGambar();
        nama         = list_bengkel.get(idx).getNama();
        alamat       = list_bengkel.get(idx).getAlamat();
        jam          = list_bengkel.get(idx).getJam();
        telepon      = list_bengkel.get(idx).getTelepon();
        email        = list_bengkel.get(idx).getEmail();
        lat          = list_bengkel.get(idx).getLat();
        lng          = list_bengkel.get(idx).getLng();

        Picasso.with(this).load(gambar).into(imgToolbar);

        toolbar.setTitle(nama);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        lblAlamat.setText(alamat);
        lblJam.setText(jam);
        lblTelepon.setText(telepon);
        lblEmail.setText(email);

        btnTlp.setOnClickListener(this);
        btnBooking.setOnClickListener(this);
        btnArah.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menu) {
        switch (menu.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(menu);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnTeleponInfo:
                AlertDialog dialog = new AlertDialog.Builder(c)
                        .setTitle("Panggilan")
                        .setMessage("Apakah anda akan melakukan panggilan ke bengkel ini?")
                        .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent phoneIntent = new Intent(Intent.ACTION_CALL);
                                phoneIntent.setData(Uri.parse("tel:" + telepon));
                                if (ActivityCompat.checkSelfPermission(c, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                    return;
                                }
                                startActivity(phoneIntent);
                            }
                        })
                        .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).create();
                dialog.show();
                break;

            case R.id.btnEmailInfo:
                Intent bookingIntent = new Intent(c, Review.class);
                bookingIntent.putExtra("id", id);
                startActivity(bookingIntent);
                finish();
                break;

            case R.id.btnDirectionInfo:
                Uri uri = Uri.parse("google.navigation:q=" + lat + "," + lng);
                Intent DirectionIntent = new Intent(Intent.ACTION_VIEW, uri);
                DirectionIntent.setPackage("com.google.android.apps.maps");
                startActivity(DirectionIntent);
                finish();
                break;
        }
    }
}
